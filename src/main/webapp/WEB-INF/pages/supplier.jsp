<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Supplier</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <a href="api/">JSON</a>
            <h1>Add supplier</h1>
            <form:form method="post" action="add" commandName="supplier" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="name">Name:</form:label>
                    <div class="controls">
                        <form:input path="name"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="email">Email:</form:label>
                    <div class="controls">
                        <form:input path="email"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="phone">Phone:</form:label>
                    <div class="controls">
                        <form:input path="phone"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="fax">Fax:</form:label>
                    <div class="controls">
                        <form:input path="fax"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="address">Address:</form:label>
                    <div class="controls">
                        <form:input path="address"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="city">City:</form:label>
                    <div class="controls">
                        <form:input path="city"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="country">Country:</form:label>
                    <div class="controls">
                        <form:input path="country"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="postalcode">Postalcode:</form:label>
                    <div class="controls">
                        <form:input path="postalcode"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add supplier" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty suppliers}">
                <h3>Suppliers</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${suppliers}" var="supplier">
                        <tr>
                            <td>${supplier.name}</td>
                            <td>${supplier.phone}</td>
                            <td>
                                <a href="api/${supplier.name}.json">JSON</a>
                                <form action="delete/${supplier.supplierId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
