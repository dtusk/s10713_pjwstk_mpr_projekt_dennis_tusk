<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Product</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <a href="api/">JSON</a>
    <div class="row">
        <div class="span8 offset2">
            <h1>Add Product</h1>
            <form:form method="post" action="add" commandName="product" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="name">Name:</form:label>
                    <div class="controls">
                        <form:input path="name"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="price">Price:</form:label>
                    <div class="controls">
                        <form:input path="price"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="amount">Amount:</form:label>
                    <div class="controls">
                        <form:input path="amount"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="description">Description:</form:label>
                    <div class="controls">
                        <form:input path="description"/>
                    </div>
                </div>


                <div class="control-group">
                    <form:label cssClass="control-label" path="width">Width:</form:label>
                    <div class="controls">
                        <form:input path="width"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="height">Height:</form:label>
                    <div class="controls">
                        <form:input path="height"/>
                    </div>
                </div>

                <div class="control-group">
                    <form:label cssClass="control-label" path="depth">Depth:</form:label>
                    <div class="controls">
                        <form:input path="depth"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" name="categoryId">Category:</label>
                    <div class="controls">
                        <select id="categoryId" name="categoryId">
                            <c:forEach items="${categories}" var="category">
                                <option value="${category.categoryId}">${category.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" name="supplierId">Supplier:</label>
                    <div class="controls">
                        <select id="supplierId" name="supplierId">
                            <c:forEach items="${suppliers}" var="supplier">
                                <option value="${supplier.supplierId}">${supplier.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add Product" class="btn">
                    </div>
                </div>
            </form:form>


            <c:if test="${!empty products}">
                <h3>Products</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Category</th>
                        <th>Supplier</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${products}" var="product">
                        <tr>
                            <td>${product.name}</td>
                            <td>${product.price}</td>
                            <td>${product.amount}</td>
                            <td>${product.category.name}</td>
                            <td>${product.supplier.name}</td>
                            <td>
                                <a href="api/${product.name}.json">JSON</a>
                                <form action="delete/${product.productId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
