<%--
  Date: 25.11.13
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Payment</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <a href="api/">json (all)</a>
        <div class="span8 offset2">
            <h1>Add Payment</h1>
            <form:form method="post" action="add" commandName="payment" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="type">Type:</form:label>
                    <div class="controls">
                        <form:input path="type"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add Payment" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty payments}">
                <h3>Payments</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${payments}" var="payment">
                        <tr>
                            <td>${payment.type}</td>
                            <td>
                                <form action="delete/${payment.paymentId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
