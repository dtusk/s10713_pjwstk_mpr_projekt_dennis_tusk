<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Order</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <a href="api">JSON (all)</a>
            <h1>Add Order</h1>
            <form:form method="post" action="add" commandName="order" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="tax">Tax:</form:label>
                    <div class="controls">
                        <form:input path="tax"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="status">Status:</form:label>
                    <div class="controls">
                        <form:input path="status"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="paid">Paid:</form:label>
                    <div class="controls">
                        <form:checkbox path="paid"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="courier">Courier:</form:label>
                    <div class="controls">
                        <select id="courierId" name="courierId">
                        <c:forEach items="${couriers}" var="courier">
                            <option value="${courier.courierId}">${courier.name}</option>
                        </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="customer">Customer:</form:label>
                    <div class="controls">
                        <select id="customerId" name="customerId">
                            <c:forEach items="${customers}" var="customer">
                                <option value="${customer.customerId}">${customer.firstName} ${customer.lastName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="payment">Payment:</form:label>
                    <div class="controls">
                        <select id="paymentId" name="paymentId">
                            <c:forEach items="${payments}" var="payment">
                                <option value="${payment.paymentId}">${payment.type}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add Customer" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty orders}">
                <h3>Orders</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Tax</th>
                        <th>Customer</th>
                        <th>Courier</th>
                        <th>Payment</th>
                        <th>Paid</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td>${order.status}</td>
                            <td>${order.tax}</td>
                            <td>${order.customer.firstName} ${order.customer.lastName}</td>
                            <td>${order.courier.name}</td>
                            <td>${order.payment.type}</td>
                            <td>${order.paid}</td>
                            <td>
                                <a href="api/${order.orderId}.json">JSON</a>
                                <form action="delete/${order.orderId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
