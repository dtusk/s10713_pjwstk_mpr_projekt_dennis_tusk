<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Ordering system</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <div class="page-header">
                <h1>Ordering system <small>by Dennis Tusk</small></h1>
            </div>
        </div>
        <h3>Links</h3>
        <ul class="list-group">
            <li class="list-group-item"><a href="courier/">Couriers</a></li> <!-- Zrobione -->
            <li class="list-group-item"><a href="product/">Products</a></li>  <!-- Zrobione -->
            <li class="list-group-item"><a href="customer/">Customers</a></li>  <!-- Zrobione -->
            <li class="list-group-item"><a href="supplier/">Suppliers</a></li>  <!-- Zrobione -->
            <li class="list-group-item"><a href="category/">Categories</a></li>  <!-- Zrobione -->
            <li class="list-group-item"><a href="payment/">Payments</a></li>   <!-- Zrobione -->
            <li class="list-group-item"><a href="order/">Orders</a></li>
            <li class="list-group-item"><a href="orderdetails/">Order Details</a></li>
        </ul>
    </div>
</div>

</body>
</html>
