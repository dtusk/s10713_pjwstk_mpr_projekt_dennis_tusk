<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Courier</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <a href="api/">JSON (all)</a>
            <h1>Add Courier</h1>
            <form:form method="post" action="add" commandName="courier" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="name">Name:</form:label>
                    <div class="controls">
                        <form:input path="name"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="phone">Phone:</form:label>
                    <div class="controls">
                        <form:input path="phone"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add Courier" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty couriers}">
                <h3>Couriers</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${couriers}" var="courier">
                        <tr>
                            <td>${courier.name}</td>
                            <td>${courier.phone}</td>
                            <td>
                                <a href="api/${courier.name}.json">JSON</a>
                                <form action="delete/${courier.courierId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
