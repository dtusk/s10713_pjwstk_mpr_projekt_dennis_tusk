<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>Customer</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <a href="api">JSON (all)</a>
            <h1>Add Customer</h1>
            <form:form method="post" action="add" commandName="customer" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="email">Email:</form:label>
                    <div class="controls">
                        <form:input path="email"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="firstName">First Name:</form:label>
                    <div class="controls">
                        <form:input path="firstName"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="lastName">Last Name:</form:label>
                    <div class="controls">
                        <form:input path="lastName"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="street">Street:</form:label>
                    <div class="controls">
                        <form:input path="street"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="postalcode">Postal code:</form:label>
                    <div class="controls">
                        <form:input path="postalcode"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="country">Country:</form:label>
                    <div class="controls">
                        <form:input path="country"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="phone">Phone:</form:label>
                    <div class="controls">
                        <form:input path="phone"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add Customer" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty customers}">
                <h3>Categories</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${customers}" var="customer">
                        <tr>
                            <td>${customer.email}</td>
                            <td>${customer.firstName}</td>
                            <td>${customer.lastName}</td>
                            <td>
                                <a href="api/${customer.email}.json">JSON</a>
                                <form action="delete/${customer.customerId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
