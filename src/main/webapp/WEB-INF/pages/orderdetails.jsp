<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>


<html>
<head>
    <meta charset="utf-8">
    <title>OrderDetails</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <div class="row">
        <div class="span8 offset2">
            <a href="api">JSON (all)</a>
            <h1>Add Order Detail</h1>
            <form:form method="post" action="add" commandName="orderdetail" class="form-horizontal">
                <div class="control-group">
                    <form:label cssClass="control-label" path="amount">Amount:</form:label>
                    <div class="controls">
                        <form:input path="amount"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="price">Price:</form:label>
                    <div class="controls">
                        <form:input path="price"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="total">Total:</form:label>
                    <div class="controls">
                        <form:input path="total"/>
                    </div>
                </div>
                <div class="control-group">
                    <form:label cssClass="control-label" path="order">Order:</form:label>
                    <div class="controls">
                        <select id="orderId" name="orderId">
                            <c:forEach items="${orders}" var="order">
                            <option value="${order.orderId}">(${order.customer.firstName} ${order.customer.lastName})</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" value="Add OrderDetail" class="btn">
                    </div>
                </div>
            </form:form>

            <c:if test="${!empty orderdetails}">
                <h3>OrderDetails</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Amount</th>
                        <th>Total</th>
                        <th>Customer</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${orderdetails}" var="orderdetail">
                        <tr>
                            <td>${orderdetail.order.orderId}</td>
                            <td>${orderdetail.amount}</td>
                            <td>${orderdetail.total}</td>
                            <td>${orderdetail.order.customer.firstName} ${orderdetail.order.customer.lastName} (id: ${orderdetail.order.customer.customerId})</td>
                            <td>
                                <a href="api/${orderdetail.orderDetailsId}.json">JSON</a>
                                <form action="delete/${orderdetail.orderDetailsId}" method="post"><input type="submit" class="btn btn-danger btn-mini" value="Delete"/></form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
