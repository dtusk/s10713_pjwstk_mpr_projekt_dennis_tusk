package com.springapp.model.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CustomHibernateDaoSupport {

    @Autowired
    private SessionFactory sessionFactory;

    public Session getHibernateTemplate() {
        if (sessionFactory.isClosed())
            return sessionFactory.openSession();
        else
            return sessionFactory.getCurrentSession();
    }

}
