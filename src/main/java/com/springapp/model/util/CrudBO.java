package com.springapp.model.util;

/**
 * @param <Class> 
 * @param <Serializable> 
 */
public interface CrudBO<Class,Serializable> {
    public void create(Class c);
    public Class read(Serializable s);
    public void update(Class c);
    public void delete(Class c);
}
