package com.springapp.model.service;

import com.springapp.model.domain.Orders;
import com.springapp.model.domain.Payment;
import com.springapp.model.util.CrudBO;

import java.util.List;


public interface PaymentBO extends CrudBO<Payment,Long> {
    /**
     * Reading all data from table Payment
     * @return
     */
    public List<Payment> readAll();
}
