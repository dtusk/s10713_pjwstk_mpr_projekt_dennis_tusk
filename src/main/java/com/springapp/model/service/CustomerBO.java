package com.springapp.model.service;

import com.springapp.model.domain.Customer;
import com.springapp.model.domain.Orders;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface CustomerBO extends CrudBO<Customer, Long> {

    /**
     * Getting all customers.
     * @return list of customers
     */
    public List<Customer> readAll();

    /**
     * Getting customer by email.
     * @param email
     * @return customer
     */
    public Customer readByEmail(String email);
}
