package com.springapp.model.service;

import com.springapp.model.domain.OrderDetails;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface OrderDetailsBO extends CrudBO<OrderDetails, Long> {
    public List<OrderDetails> readAll();
}
