package com.springapp.model.service;


import com.springapp.model.domain.Product;
import com.springapp.model.domain.Supplier;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface ProductBO extends CrudBO<Product, Long> {
    public List<Product> readAll();
    public Product readByName(String name);
}
