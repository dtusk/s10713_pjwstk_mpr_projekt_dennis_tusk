package com.springapp.model.service;

import com.springapp.model.domain.Category;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface CategoryBO extends CrudBO<Category, Long> {
    public List<Category> readAll();
    public Category readByName(String name);
}
