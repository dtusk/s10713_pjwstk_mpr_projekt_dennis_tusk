package com.springapp.model.service;

import com.springapp.model.domain.Courier;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface CourierBO extends CrudBO<Courier,Long> {
    public Courier findByName(String name);
    public Courier findByPhoneNumber(int phone);
    public List<Courier> readAll();
}
