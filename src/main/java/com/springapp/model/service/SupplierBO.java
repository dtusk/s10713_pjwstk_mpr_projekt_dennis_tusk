package com.springapp.model.service;

import com.springapp.model.domain.Product;
import com.springapp.model.domain.Supplier;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface SupplierBO extends CrudBO<Supplier,Long> {
    public Supplier readByName(String name);
    public List<Supplier> readAll();
    public List<Product> readAllProducts(Supplier supplier);
}
