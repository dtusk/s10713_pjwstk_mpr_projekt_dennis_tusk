package com.springapp.model.service.impl;

import com.springapp.model.domain.Orders;
import com.springapp.model.service.PaymentBO;
import com.springapp.model.domain.Payment;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("paymentBO")
@Transactional
public class PaymentBOImpl extends CustomHibernateDaoSupport implements PaymentBO {
    @Override
    public void create(Payment c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Payment read(Long s) {
        return (Payment) getHibernateTemplate().get(Payment.class, s);
    }

    @Override
    public void update(Payment c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Payment c) {
        getHibernateTemplate().delete(c);
    }

    @Override
    public List<Payment> readAll() {
        return getHibernateTemplate().createQuery("from Payment").list();
    }
}
