package com.springapp.model.service.impl;

import com.springapp.model.service.CustomerBO;
import com.springapp.model.domain.Customer;
import com.springapp.model.domain.Orders;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("customerBO")
@Transactional
public class CustomerBOImpl extends CustomHibernateDaoSupport implements CustomerBO {

    @Override
    public List<Customer> readAll() {
        return getHibernateTemplate().createQuery("from Customer").list();
    }

    @Override
    public Customer readByEmail(String email) {
        Query q = getHibernateTemplate().createQuery("from Customer where email = :email");
        q.setParameter("email", email);
        return (Customer) q.list().get(0);
    }

    @Override
    public void create(Customer c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Customer read(Long s) {
        return (Customer) getHibernateTemplate().get(Customer.class, s);
    }

    @Override
    public void update(Customer c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Customer c) {
        getHibernateTemplate().delete(c);
    }
}
