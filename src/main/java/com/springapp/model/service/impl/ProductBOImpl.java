package com.springapp.model.service.impl;

import com.springapp.model.domain.Product;
import com.springapp.model.domain.Supplier;
import com.springapp.model.service.ProductBO;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("productBO")
@Transactional
public class ProductBOImpl extends CustomHibernateDaoSupport implements ProductBO {
    @Override
    public List<Product> readAll() {
        Query q = getHibernateTemplate().createQuery("from Product");
        return q.list();
    }

    @Override
    public Product readByName(String name) {
        Query q = getHibernateTemplate().createQuery("from Product where name = :name");
        q.setParameter("name", name);
        return (Product) q.list().get(0);
    }

    @Override
    public void create(Product c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Product read(Long s) {
        return (Product) getHibernateTemplate().get(Product.class, s);
    }

    @Override
    public void update(Product c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Product c) {
        getHibernateTemplate().delete(c);
    }
}
