package com.springapp.model.service.impl;

import com.springapp.model.domain.Orders;
import com.springapp.model.service.OrderBO;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("orderBO")
@Transactional
public class OrderBOImpl extends CustomHibernateDaoSupport implements OrderBO {
    @Override
    public List<Orders> readAll() {
        return getHibernateTemplate().createQuery("from Orders").list();
    }

    @Override
    public void create(Orders c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Orders read(Long s) {
        return (Orders) getHibernateTemplate().get(Orders.class, s);
    }

    @Override
    public void update(Orders c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Orders c) {
        getHibernateTemplate().delete(c);
    }
}
