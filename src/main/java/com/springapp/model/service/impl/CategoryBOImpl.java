package com.springapp.model.service.impl;

import com.springapp.model.domain.Category;
import com.springapp.model.service.CategoryBO;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("categoryBO")
@Transactional
public class CategoryBOImpl extends CustomHibernateDaoSupport implements CategoryBO {
    @Override
    public List<Category> readAll() {
        return getHibernateTemplate().createQuery("from Category").list();
    }

    @Override
    public Category readByName(String name) {
        Query q = getHibernateTemplate().createQuery("from Category where name = :name");
        q.setParameter("name", name);
        return (Category) q.list().get(0);
    }

    @Override
    public void create(Category c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Category read(Long s) {
        return (Category) getHibernateTemplate().get(Category.class, s);
    }

    @Override
    public void update(Category c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Category c) {
        getHibernateTemplate().delete(c);
    }
}
