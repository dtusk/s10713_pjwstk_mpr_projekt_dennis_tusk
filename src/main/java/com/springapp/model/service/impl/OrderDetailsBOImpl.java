package com.springapp.model.service.impl;

import com.springapp.model.domain.OrderDetails;
import com.springapp.model.service.OrderDetailsBO;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("orderDetailsBO")
@Transactional
public class OrderDetailsBOImpl extends CustomHibernateDaoSupport implements OrderDetailsBO {
    @Override
    public List<OrderDetails> readAll() {
        return getHibernateTemplate().createQuery("from OrderDetails").list();
    }

    @Override
    public void create(OrderDetails c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public OrderDetails read(Long s) {
        return (OrderDetails) getHibernateTemplate().get(OrderDetails.class, s);
    }

    @Override
    public void update(OrderDetails c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(OrderDetails c) {
        getHibernateTemplate().delete(c);
    }
}
