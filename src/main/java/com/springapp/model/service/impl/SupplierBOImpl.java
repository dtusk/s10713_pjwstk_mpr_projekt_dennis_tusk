package com.springapp.model.service.impl;

import com.springapp.model.domain.Product;
import com.springapp.model.domain.Supplier;
import com.springapp.model.service.SupplierBO;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("supplierBO")
@Transactional
public class SupplierBOImpl extends CustomHibernateDaoSupport implements SupplierBO {
    @Override
    public Supplier readByName(String name) {
        Query q = getHibernateTemplate().createQuery("from Supplier where name = :name");
        q.setParameter("name", name);
        return (Supplier) q.list().get(0);
    }

    @Override
    public List<Supplier> readAll() {
        return getHibernateTemplate().createQuery("from Supplier").list();
    }

    @Override
    public List<Product> readAllProducts(Supplier supplier) {
        Query q = getHibernateTemplate().createQuery("from Product where supplierId = :supplierId");
        q.setParameter("supplierId", supplier.getSupplierId());
        return q.list();
    }

    @Override
    public void create(Supplier c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Supplier read(Long s) {
        return (Supplier) getHibernateTemplate().get(Supplier.class, s);
    }

    @Override
    public void update(Supplier c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Supplier c) {
        getHibernateTemplate().delete(c);
    }
}
