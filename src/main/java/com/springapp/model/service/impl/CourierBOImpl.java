package com.springapp.model.service.impl;

import java.util.List;

import com.springapp.model.service.CourierBO;
import com.springapp.model.domain.Courier;
import com.springapp.model.util.CustomHibernateDaoSupport;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("courierBO")
@Transactional
public class CourierBOImpl extends CustomHibernateDaoSupport implements CourierBO {

    @Override
    public Courier findByName(String name) {
        Query q = getHibernateTemplate().createQuery("from Courier where name = :name");
        q.setParameter("name", name);
        return (Courier) q.list().get(0);
    }

    @Override
    public List<Courier> readAll() {
        return getHibernateTemplate().createQuery("from Courier").list();
    }

    @Override
    public void create(Courier c) {
        getHibernateTemplate().save(c);
    }

    @Override
    public Courier read(Long s) {
        return (Courier) getHibernateTemplate().get(Courier.class, s);
    }

    @Override
    public void update(Courier c) {
        getHibernateTemplate().update(c);
    }

    @Override
    public void delete(Courier c) {
        getHibernateTemplate().delete(c);
    }

    @Override
    public Courier findByPhoneNumber(int phone) {
        Query q = getHibernateTemplate().createQuery("from Courier where phone = :phone");
        q.setParameter("phone", phone);
        return (Courier) q.list().get(0);
    }

    
}
