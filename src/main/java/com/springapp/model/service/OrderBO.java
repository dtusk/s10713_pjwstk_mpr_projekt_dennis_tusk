package com.springapp.model.service;


import com.springapp.model.domain.Orders;
import com.springapp.model.util.CrudBO;

import java.util.List;

public interface OrderBO extends CrudBO<Orders, Long> {
    public List<Orders> readAll();
}
