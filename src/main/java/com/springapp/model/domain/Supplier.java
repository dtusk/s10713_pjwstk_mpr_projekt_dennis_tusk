package com.springapp.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "Supplier")
public class Supplier implements Serializable {

    private Long supplierId;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(unique = true)
    private int fax;
    private String name, address, city, country, postalcode;
    @Column(unique = true)
    private int phone;
    private Set<Product> product;


    public Supplier() {
        super();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFax() {
        return fax;
    }

    public void setFax(int fax) {
        this.fax = fax;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    @OneToMany(fetch = FetchType.EAGER)
    public Set<Product> getProducts() {
        return product;
    }

    public void setProducts(Set<Product> product) {
        this.product = product;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "supplierId=" + supplierId +
                ", email='" + email + '\'' +
                ", fax=" + fax +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", postalcode='" + postalcode + '\'' +
                ", phone=" + phone +
                ", product=" + product +
                '}';
    }
}
