package com.springapp.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
@Entity
@Table(name = "Courier")
public class Courier implements Serializable {

    private Long courierId;
    @Column(unique = true, nullable = false)
    private String name;
    private int phone;

    private Set<Orders> order;

    public Courier() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getCourierId() {
        return courierId;
    }

    public void setCourierId(Long courierId) {
        this.courierId = courierId;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


    @OneToMany(fetch = FetchType.EAGER)
    public Set<Orders> getOrder() {
		return order;
	}

	public void setOrder(Set<Orders> order) {
		this.order = order;
	}

    @Column(unique = true, nullable = false)
	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

    @Override
    public String toString() {
        return "Courier{" +
                "courierId=" + courierId +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", phone=" + phone +
                '}';
    }
}