package com.springapp.model.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Orders")
public class Orders implements Serializable {

    private Long orderId;
    @Column(nullable = false)
	private float tax;
	private String status;
	private Date timestamp;
	private boolean paid;
    private Courier courier;
    private Customer customer;
    private Payment payment;

	private Set<OrderDetails> orderdetails;
	
	public Orders() {
		super();
	}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public float getTax() {
		return tax;
	}

	public void setTax(float tax) {
		this.tax = tax;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<OrderDetails> getOrderdetails() {
		return orderdetails;
	}

	public void setOrderdetails(Set<OrderDetails> orderdetails) {
		this.orderdetails = orderdetails;
	}

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

	public Date getTimestamp() {
		return timestamp;
	}

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "courierId", nullable = false)
    public Courier getCourier() {
        return courier;
    }

    public void setCourier(Courier courier) {
        this.courier = courier;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customerId")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "paymentId")
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
