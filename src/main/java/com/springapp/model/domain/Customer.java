package com.springapp.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "Customer")
public class Customer implements Serializable {

    private Long customerId;
    @Column(nullable = false, unique = true)
	private String email;
    @Column(nullable = false)
	private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String street;
    @Column(nullable = false)
    private String postalcode;
    @Column(nullable = false)
    private String city;
    @Column(nullable = false)
    private String country;

	private Set<Orders> order;
    @Column(nullable = false, unique = true)
	private int phone;

	public Customer() {
		super();
    }

    public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
	public Set<Orders> getOrder() {
		return order;
	}

	public int getPhone() {
		return phone;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public String getStreet() {
		return street;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setOrder(Set<Orders> order) {
		this.order = order;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public void setStreet(String street) {
		this.street = street;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

}
