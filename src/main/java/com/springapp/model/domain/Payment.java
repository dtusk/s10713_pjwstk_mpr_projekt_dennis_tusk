package com.springapp.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Payment")
public class Payment implements Serializable {

	private Long paymentId;
	private String type;

	private Set<Orders> order;

	public Payment() {
		super();
	}

	public Payment(String type) {
		super();
		this.type = type;
	}

	public Payment(String type, Set<Orders> order) {
		super();
		this.type = type;
		this.order = order;
	}

    @Column(nullable = false, unique = true)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    @OneToMany(fetch = FetchType.EAGER)
	public Set<Orders> getOrder() {
		return order;
	}

	public void setOrder(Set<Orders> order) {
		this.order = order;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
}
