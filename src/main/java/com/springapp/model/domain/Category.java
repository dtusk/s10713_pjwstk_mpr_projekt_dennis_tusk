package com.springapp.model.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "Category")
public class Category implements Serializable {

    private Long categoryId;
    @Column(nullable = false, unique = true)
    private String name;
    private String description;

	private Set<Product> products;

	public Category() {

	}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

    @OneToMany(fetch = FetchType.EAGER)
	public Set<Product> getProducts() {
		return products;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

}
