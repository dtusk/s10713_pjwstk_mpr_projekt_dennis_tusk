package com.springapp.model.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "OrderDetails")
public class OrderDetails implements Serializable {

    private Long orderDetailsId;
	private double price;
	private int amount;
	private double total;
    private Orders order;

    public OrderDetails() {
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "orderId")
    public Orders getOrder() {
        return order;
    }
	
    public void setOrder(Orders order) {
        this.order = order;
    }
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(Long orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }
}
