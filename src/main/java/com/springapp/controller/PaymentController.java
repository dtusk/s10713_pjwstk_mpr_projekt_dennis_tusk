package com.springapp.controller;

import com.springapp.model.service.PaymentBO;
import com.springapp.model.domain.Payment;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/payment")
public class PaymentController {
    @Autowired
    private PaymentBO paymentBO;

    @RequestMapping("/")
    public String showPayments(ModelMap model) {
        model.addAttribute("payment", new Payment());
        model.addAttribute("payments", paymentBO.readAll());
        return "payment";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String createPayment(@ModelAttribute("payment") Payment payment) {
        paymentBO.create(payment);
        return "redirect:/payment/";
    }

    @RequestMapping("/delete/{paymentId}")
    public String deletePayment(@PathVariable("paymentId") Long paymentId) {
        Payment payment = paymentBO.read(paymentId);
        paymentBO.delete(payment);
        return "redirect:/payment/";
    }

    /*

    API

     */

    @RequestMapping("/api")
    public @ResponseBody String readAllJson() {
        JSONArray paymentArray = new JSONArray();
        for (Payment payment : paymentBO.readAll()) {
            JSONObject paymentObject = new JSONObject();
            paymentObject.put("paymentId", payment.getPaymentId());
            paymentObject.put("type", payment.getType());
            paymentArray.put(paymentObject);
        }
        return paymentArray.toString();
    }
}
