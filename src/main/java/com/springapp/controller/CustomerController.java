package com.springapp.controller;

import com.springapp.model.service.CustomerBO;
import com.springapp.model.domain.Customer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerBO customerBO;

    @RequestMapping("/")
    public String showCustomer(ModelMap model) {
        model.addAttribute("customer", new Customer());
        model.addAttribute("customers", customerBO.readAll());
        return "customer";
    }

    @RequestMapping(value="add", method = RequestMethod.POST)
    public String addCustomer(@ModelAttribute("customer") Customer customer, BindingResult result) {
        customerBO.create(customer);
        return "redirect:/customer/";
    }

    @RequestMapping("delete/{customerId}")
    public String deleteCustomer(@PathVariable("customerId") Long customerId) {
        customerBO.delete(customerBO.read(customerId));
        return "redirect:/customer/";
    }

    /*

    API

     */

    @RequestMapping("api")
    public @ResponseBody String readAllJson() {
        JSONArray jsonArray = new JSONArray();
        for (Customer customer : customerBO.readAll()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("customerId", customer.getCustomerId());
            jsonObject.put("firstName", customer.getFirstName());
            jsonObject.put("lastName", customer.getLastName());
            jsonObject.put("phone", customer.getPhone());
            jsonObject.put("email", customer.getEmail());
            jsonObject.put("street", customer.getStreet());
            jsonObject.put("city", customer.getCity());
            jsonObject.put("postalcode", customer.getPostalcode());
            jsonObject.put("country", customer.getCountry());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }

    @RequestMapping("api/{email}")
    public @ResponseBody String readByEmailJson(@PathVariable("email") String email) {
        Customer customer = customerBO.readByEmail(email);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("customerId", customer.getCustomerId());
            jsonObject.put("firstName", customer.getFirstName());
            jsonObject.put("lastName", customer.getLastName());
            jsonObject.put("phone", customer.getPhone());
            jsonObject.put("email", customer.getEmail());
            jsonObject.put("street", customer.getStreet());
            jsonObject.put("city", customer.getCity());
            jsonObject.put("postalcode", customer.getPostalcode());
            jsonObject.put("country", customer.getCountry());
        return jsonObject.toString();
    }

}
