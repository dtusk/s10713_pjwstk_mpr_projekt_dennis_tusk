package com.springapp.controller;

import com.springapp.model.domain.OrderDetails;
import com.springapp.model.service.OrderBO;
import com.springapp.model.service.OrderDetailsBO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/orderdetails")
public class OrderDetailsController {

    @Autowired
    private OrderDetailsBO orderDetailsBO;

    @Autowired
    private OrderBO orderBO;

    @RequestMapping("/")
    public String showOrderDetails(ModelMap model) {
        model.addAttribute("orderdetail", new OrderDetails());
        model.addAttribute("orders", orderBO.readAll());
        model.addAttribute("orderdetails", orderDetailsBO.readAll());
        return "orderdetails";
    }

    @RequestMapping( value = "/add", method = RequestMethod.POST)
    public String createOrderDetail(@ModelAttribute OrderDetails orderDetails, @RequestParam long orderId) {
        orderDetails.setOrder(orderBO.read(orderId));
        orderDetailsBO.create(orderDetails);
        return "redirect:/orderdetails/";
    }

    @RequestMapping("/delete/{orderDetailsId}")
    public String deleteOrderDetail(@PathVariable("orderDetailsId") long orderDetailsId) {
        orderDetailsBO.delete(orderDetailsBO.read(orderDetailsId));
        return "redirect:/orderdetails";
    }

    /*

    API /REST

     */

    @RequestMapping("/api")
    public @ResponseBody String readAllJson() {
        JSONArray jsonArray = new JSONArray();
        for (OrderDetails orderDetails : orderDetailsBO.readAll()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("orderDetailsId",orderDetails.getOrderDetailsId());
            jsonObject.put("amount",orderDetails.getAmount());
            jsonObject.put("price", orderDetails.getPrice());
            jsonObject.put("total", orderDetails.getTotal());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }


    @RequestMapping("/api/{orderDetailsId}")
    public @ResponseBody String readJson(@PathVariable("orderDetailsId") long orderDetailsId) {
        OrderDetails orderDetails = orderDetailsBO.read(orderDetailsId);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("orderDetailsId",orderDetails.getOrderDetailsId());
            jsonObject.put("amount",orderDetails.getAmount());
            jsonObject.put("price", orderDetails.getPrice());
            jsonObject.put("total", orderDetails.getTotal());
        return jsonObject.toString();
    }

}
