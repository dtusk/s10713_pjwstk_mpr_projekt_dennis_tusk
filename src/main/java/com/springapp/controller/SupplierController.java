package com.springapp.controller;

import com.springapp.model.domain.Supplier;
import com.springapp.model.service.SupplierBO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierBO supplierBO;

    @RequestMapping({"","/"})
    public String showSuppliers(ModelMap model) {
        model.addAttribute("supplier", new Supplier());
        model.addAttribute("suppliers", supplierBO.readAll());
        return "supplier";
    }

    @RequestMapping(value = {"add","/add"}, method = RequestMethod.POST)
    public String createSupplier(@ModelAttribute("supplier") Supplier supplier, BindingResult result) {
        supplierBO.create(supplier);
        return "redirect:/supplier/";
    }

    @RequestMapping("/delete/{supplierId}")
    public String deleteSupplier(@PathVariable("supplierId") Long supplierId) {
        supplierBO.delete(supplierBO.read(supplierId));
        return "redirect:/supplier/";
    }

    @RequestMapping("/api/")
    public @ResponseBody String getAllJson() {
        JSONArray supplierArray = new JSONArray();
        for (Supplier supplier : supplierBO.readAll()) {
            JSONObject supplierObject = new JSONObject();
            supplierObject.put("supplierId", supplier.getSupplierId());
            supplierObject.put("name", supplier.getName());
            supplierObject.put("email", supplier.getEmail());
            supplierObject.put("address", supplier.getAddress());
            supplierObject.put("city", supplier.getCity());
            supplierObject.put("postalcode", supplier.getPostalcode());
            supplierObject.put("country",supplier.getCountry());
            supplierObject.put("fax",supplier.getFax());
            supplierObject.put("phone", supplier.getPhone());
            supplierArray.put(supplierObject);
        }
        return supplierArray.toString();
    }

    @RequestMapping("/api/{name}.json")
    public @ResponseBody String getNameJson(@PathVariable("name") String name) {
        JSONObject supplierObject = new JSONObject();
        Supplier supplier = supplierBO.readByName(name);
            supplierObject.put("supplierId", supplier.getSupplierId());
            supplierObject.put("name", supplier.getName());
            supplierObject.put("email", supplier.getEmail());
            supplierObject.put("address", supplier.getAddress());
            supplierObject.put("city", supplier.getCity());
            supplierObject.put("postalcode", supplier.getPostalcode());
            supplierObject.put("country",supplier.getCountry());
            supplierObject.put("fax",supplier.getFax());
            supplierObject.put("phone", supplier.getPhone());
        return supplierObject.toString();
    }

}
