package com.springapp.controller;

import com.springapp.model.service.CourierBO;
import com.springapp.model.domain.Courier;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/courier")
public class CourierController {

    private static Logger logger = Logger.getLogger(CourierController.class);

    @Autowired
    private CourierBO courierBO;

    @RequestMapping("/")
    public String showCouriers(ModelMap model) {
        model.addAttribute("courier", new Courier());
        model.addAttribute("couriers", courierBO.readAll());
        if (logger.isInfoEnabled())
            logger.info("Courier Add/Delete section");
        return "courier";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addCourier(@ModelAttribute("courier") Courier courier, BindingResult result) {
        courierBO.create(courier);
        if (logger.isInfoEnabled())
            logger.info("Added courier: " + courier);
        return "redirect:/courier/";
    }

    @RequestMapping("/delete/{courierId}")
    public String deleteCourier(@PathVariable("courierId") Long courierId) {
        Courier courier = courierBO.read(courierId);
        courierBO.delete(courier);
        if (logger.isInfoEnabled())
            logger.info("Deleted courier " + courier);
        return "redirect:/courier/";
    }

    /*

    API

     */


    @RequestMapping(value="/api/", method = RequestMethod.GET)
    public @ResponseBody String getJSONAll () {
        JSONArray courierArray = new JSONArray();
        for (Courier courier: courierBO.readAll()) {
            JSONObject courierObject = new JSONObject();
            courierObject.put("courierId", courier.getCourierId());
            courierObject.put("name", courier.getName());
            courierObject.put("phone", courier.getPhone());
            courierArray.put(courierObject);
        }
        if (logger.isInfoEnabled())
            logger.info("Showing all couriers in JSON format");
        return courierArray.toString();
    }

    @RequestMapping(value="/api/{name}", method = RequestMethod.GET)
    public @ResponseBody String getJSON (@PathVariable("name") String name) {
        Courier result = courierBO.findByName(name);
        JSONObject courierObject = new JSONObject();
        courierObject.put("courierId", result.getCourierId());
        courierObject.put("name", result.getName());
        courierObject.put("phone", result.getPhone());
        if (logger.isInfoEnabled())
            logger.info("Showing specific courier in JSON format: " + result);
        return courierObject.toString();
    }

}
