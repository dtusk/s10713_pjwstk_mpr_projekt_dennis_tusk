package com.springapp.controller;

import com.springapp.model.domain.Product;
import com.springapp.model.service.CategoryBO;
import com.springapp.model.service.ProductBO;
import com.springapp.model.service.SupplierBO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductBO productBO;

    @Autowired
    private SupplierBO supplierBO;

    @Autowired
    private CategoryBO categoryBO;

    @RequestMapping({"/",""})
    public String showProducts(ModelMap model) {
        model.addAttribute("product", new Product());
        model.addAttribute("products", productBO.readAll());
        model.addAttribute("categories", categoryBO.readAll());
        model.addAttribute("suppliers", supplierBO.readAll());
        return "product";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String createProduct(@ModelAttribute("product") Product product, @RequestParam Long categoryId, @RequestParam Long supplierId, BindingResult result, ModelMap model, SessionStatus status, HttpSession session) {
        if (result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors());
            model.addAttribute("debug", session.getAttribute("supplierId"));
        } else {
            if (null != supplierId && null != categoryId) {
                product.setCategory(categoryBO.read(categoryId));
                product.setSupplier(supplierBO.read(supplierId));
                productBO.create(product);
                status.setComplete();
            }
        }
        return "redirect:/product/";
    }

    @RequestMapping("/delete/{productId}")
    public String deleteProduct(@PathVariable long productId) {
        productBO.delete(productBO.read(productId));
        return "redirect:/product/";
    }

    /*

    API

     */

    @RequestMapping("/api/")
    public @ResponseBody String getAllJson() {
        JSONArray jsonArray = new JSONArray();
        for (Product product : productBO.readAll()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("productId",product.getProductId());
            jsonObject.put("amount", product.getAmount());
            jsonObject.put("price", product.getPrice());
            jsonObject.put("name", product.getName());
            jsonObject.put("width", product.getWidth());
            jsonObject.put("height", product.getHeight());
            jsonObject.put("depth", product.getDepth());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }

    @RequestMapping("/api/{name}.json")
    public @ResponseBody String getProductJson(@PathVariable String name) {
        Product product = productBO.readByName(name);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("productId",product.getProductId());
            jsonObject.put("amount", product.getAmount());
            jsonObject.put("price", product.getPrice());
            jsonObject.put("name", product.getName());
            jsonObject.put("width", product.getWidth());
            jsonObject.put("height", product.getHeight());
            jsonObject.put("depth", product.getDepth());
        return jsonObject.toString();
    }

}
