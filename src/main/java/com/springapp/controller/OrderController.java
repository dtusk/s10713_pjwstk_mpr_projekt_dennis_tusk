package com.springapp.controller;

import com.springapp.model.domain.Orders;
import com.springapp.model.service.CourierBO;
import com.springapp.model.service.CustomerBO;
import com.springapp.model.service.OrderBO;
import com.springapp.model.service.PaymentBO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderBO orderBO;

    @Autowired
    private CourierBO courierBO;

    @Autowired
    private CustomerBO customerBO;

    @Autowired
    private PaymentBO paymentBO;


    @RequestMapping("/")
    public String showOrders(ModelMap model) {
        model.addAttribute("order", new Orders());
        model.addAttribute("orders", orderBO.readAll());
        model.addAttribute("couriers", courierBO.readAll());
        model.addAttribute("customers", customerBO.readAll());
        model.addAttribute("payments", paymentBO.readAll());
        return "order";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String createOrder(@ModelAttribute Orders order, @RequestParam long courierId, @RequestParam long customerId, @RequestParam long paymentId) {
        order.setCourier(courierBO.read(courierId));
        order.setCustomer(customerBO.read(customerId));
        order.setPayment(paymentBO.read(paymentId));
        orderBO.create(order);
        return "redirect:/order/";
    }

    @RequestMapping("delete/{orderId}")
    public String deleteOrder(@PathVariable("orderId") long orderId) {
        orderBO.delete(orderBO.read(orderId));
        return "redirect:/order/";
    }

    /*

    REST/API ...

     */

    @RequestMapping("api")
    public @ResponseBody String readAllJson() {
        JSONArray orderArray = new JSONArray();
        for (Orders order : orderBO.readAll()) {
            JSONObject orderObject = new JSONObject();
            orderObject.put("orderId", order.getOrderId());
            orderObject.put("status", order.getStatus());
            orderObject.put("tax",order.getTax());
            orderObject.put("paid", order.isPaid());
            orderObject.put("date", order.getTimestamp());
            orderArray.put(orderObject);
        }
        return orderArray.toString();
    }

    @RequestMapping("api/{orderId}.json")
    public @ResponseBody String readJson(@PathVariable("orderId") long orderId) {
        Orders order = orderBO.read(orderId);
        JSONObject orderObject = new JSONObject();
        orderObject.put("orderId", order.getOrderId());
        orderObject.put("status", order.getStatus());
        orderObject.put("tax",order.getTax());
        orderObject.put("paid", order.isPaid());
        orderObject.put("date", order.getTimestamp());
        return orderObject.toString();
    }



}
