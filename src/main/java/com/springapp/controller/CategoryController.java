package com.springapp.controller;

import com.springapp.model.domain.Category;
import com.springapp.model.service.CategoryBO;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryBO categoryBO;


    @RequestMapping({"/",""})
    public String manageCategories(ModelMap model) {
        model.addAttribute("category", new Category());
        model.addAttribute("categories", categoryBO.readAll());
        return "category";
    }

    @RequestMapping(value = {"add","/add"}, method = RequestMethod.POST)
    public String createCategory(@ModelAttribute Category category, ModelMap model, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("errors", result.getAllErrors());
        } else {
            categoryBO.create(category);
        }
        return "redirect:/category/";
    }

    @RequestMapping({"/delete/{categoryId}","delete/{categoryId}"})
    public String deleteCategory(@PathVariable("categoryId") long categoryId) {
        Category cat = categoryBO.read(categoryId);
        categoryBO.delete(cat);
        return "redirect:/category/";
    }

    /*

    API

     */

    @RequestMapping({"/api","api"})
    public @ResponseBody String readAllJson() {
        JSONArray jsonArray = new JSONArray();
        for (Category category : categoryBO.readAll()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("categoryId", category.getCategoryId());
            jsonObject.put("name", category.getName());
            jsonObject.put("description", category.getDescription());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }

    @RequestMapping({"/api/{name}","api/{name}"})
    public @ResponseBody String readJson(@PathVariable("name") String name) {
        Category category = categoryBO.readByName(name);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("categoryId", category.getCategoryId());
            jsonObject.put("name", category.getName());
            jsonObject.put("description", category.getDescription());
        return jsonObject.toString();
    }


}
