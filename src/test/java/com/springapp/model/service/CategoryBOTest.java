package com.springapp.model.service;

import com.springapp.model.domain.Category;
import com.springapp.model.domain.Product;
import com.springapp.model.util.CustomHibernateDaoSupport;
import junit.framework.TestCase;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class CategoryBOTest extends TestCase {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String FIND_NAME = "Nazwa kategorii";

    @Autowired
    private  CategoryBO  categoryBO;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {
        Category c1 = new Category();
        c1.setName(FIND_NAME);
        c1.setDescription("Pierwszy opis produktu");
        Category c2 = new Category();
        c2.setName("Druga nazwa kategorii");
        c2.setDescription("Drugi opis kategorii");
        sessionFactory.openSession().save(c1);
        sessionFactory.openSession().save(c2);
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Category");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, categoryBO.readAll().size());
    }

    @Test
    public void testReadByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name FROM Category WHERE name = '" + FIND_NAME + "'");
        String result = "";
        if (rs.next())
            result = rs.getString("name");
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, categoryBO.readByName(FIND_NAME).getName());
    }
}
