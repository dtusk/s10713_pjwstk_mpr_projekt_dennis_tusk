package com.springapp.model.service;

import com.springapp.model.domain.Courier;
import com.springapp.model.domain.OrderDetails;
import com.springapp.model.domain.Orders;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class OrderDetailsBOTest {
    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";;

    @Autowired
    private OrderDetailsBO orderDetailsBO;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setAmount(0);
        sessionFactory.openSession().save(orderDetails);
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE OrderDetails"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM OrderDetails");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, orderDetailsBO.readAll().size());
    }
}
