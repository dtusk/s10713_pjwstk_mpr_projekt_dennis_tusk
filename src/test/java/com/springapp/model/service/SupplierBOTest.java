package com.springapp.model.service;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class SupplierBOTest extends TestCase {

    @Autowired
    private SupplierBO supplierBO;

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String READBYNAME_ADDRESS = "Gdańska";
    private final String READBYNAME_NAME = "Biedra";

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO Supplier (address, city, country, email, fax, name, phone, postalcode) VALUES ('" + READBYNAME_ADDRESS + "', 'Warszawa', 'Poland', 'kosz@biedra.pl', 123456789, '" + READBYNAME_NAME + "', 123456789, '12-345' )");
        stmt.executeUpdate("INSERT INTO Supplier (address, city, country, email, fax, name, phone, postalcode) VALUES ('13 Ulica', 'Nowy York', 'Murica', '987@0123.com', 987654321, '0123 Service', 987654321, 'Murykanski' )");
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Supplier"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testReadByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT address FROM Supplier WHERE name = '" + READBYNAME_NAME +"'");
        String result = "";
        if (rs.next())
            result = rs.getString(1);
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, supplierBO.readByName(READBYNAME_NAME).getAddress());
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Supplier");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, supplierBO.readAll().size());
    }

    @Test
    public void testReadAllProducts() throws Exception {
        assertTrue(true);
    }
}
