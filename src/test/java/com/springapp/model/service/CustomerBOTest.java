package com.springapp.model.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static junit.framework.Assert.*;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class CustomerBOTest {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String C1_FNAME = "Jan", C1_LNAME = "Kowalski";

    @Autowired
    private CustomerBO customerBO;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO Customer (city, country, email, firstName, lastName, phone, postalcode, street) VALUES ('Warszawa', 'Poland', 'jakis@tam.pl', '" + C1_FNAME + "', '" + C1_LNAME + "', 123456789, 01-234, 'Gdanska')");
        stmt.executeUpdate("INSERT INTO Customer (city, country, email, firstName, lastName, phone, postalcode, street) VALUES ('Warszawa', 'Poland', 'jakis@tam.pl', 'Jakis', 'Tam', 987654321, 01-234, 'Gdanska')");
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Customer"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Customer");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, customerBO.readAll().size());
    }

}
