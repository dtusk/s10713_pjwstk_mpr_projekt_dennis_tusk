package com.springapp.model.service;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class PaymentBOTest extends TestCase {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String TYPE_PAYPAL = "PayPal";
    private final String TYPE_MASTERCARD = "Mastercard";

    @Autowired
    private PaymentBO paymentBO;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO Payment (type) VALUES ('" + TYPE_PAYPAL + "')");
        stmt.executeUpdate("INSERT INTO Payment (type) VALUES ('" + TYPE_MASTERCARD + "')");
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Payment"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Payment");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, paymentBO.readAll().size());
    }

    @Test
    public void testReadAllOrders() throws Exception {
        assertTrue(true);
    }
}
