package com.springapp.model.service;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class ProductBOTest extends TestCase {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String PRODUCT_NAME = "ekspres";

    @Autowired
    private ProductBO productBO;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO Product (name, amount, depth, width, height, price) VALUES ('" + PRODUCT_NAME + "', 1000, 0.0,0.0,0.0,1.0)");
        stmt.executeUpdate("INSERT INTO Product (name, amount, depth, width, height, price) VALUES ('Produkt', 1000, 0.0,0.0,0.0,1.0)");
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Product"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }
    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Product");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, productBO.readAll().size());
    }

    @Test
    public void testReadByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name FROM Product where name = '"+ PRODUCT_NAME + "'");
        String result = "";
        if (rs.next())
            result = rs.getString("name");
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, productBO.readByName(PRODUCT_NAME).getName());
    }
}
