package com.springapp.model.service;

import com.springapp.model.domain.Courier;
import com.springapp.model.domain.Orders;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class OrderBOTest {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";;
    private final float TAX = 123456789.0f;

    @Autowired
    private OrderBO orderBO;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() throws Exception {
        Orders order = new Orders();
        order.setTax(TAX);
        Orders order1 = new Orders();
        order1.setTax(11.0f);
        Courier courier = new Courier();
        courier.setName("Ósemka");
        order.setCourier(courier);
        order1.setCourier(courier);
        sessionFactory.openSession().save(order);
        sessionFactory.openSession().save(order1);
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Orders"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Orders");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, orderBO.readAll().size());
    }
}
