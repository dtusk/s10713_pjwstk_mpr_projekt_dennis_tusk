package com.springapp.model.service;

import junit.framework.TestCase;
import static junit.framework.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class CourierBOTest extends TestCase {

    private final String JDBC_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String FIND_NAME = "Ósemka";
    private final int FIND_PHONE = 123456789;

    @Autowired
    private CourierBO courierBO;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO Courier (name, phone) VALUES ('" + FIND_NAME + "', '" + FIND_PHONE + "')");
        stmt.executeUpdate("INSERT INTO Courier (name, phone) VALUES ('JanCourier', '987654321')");
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                "TRUNCATE TABLE Courier"
        );
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
    }

    @Test
    public void testFindByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name FROM Courier WHERE name = '" + FIND_NAME + "'");
        String result = "";
        if (rs.next())
            result = rs.getString(1);
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, courierBO.findByName(FIND_NAME).getName());
    }

    @Test
    public void testFindByPhoneNumber() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT phone FROM Courier WHERE phone = '" + FIND_PHONE + "'");
        int result = 0;
        if (rs.next())
            result = rs.getInt(1);
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, courierBO.findByPhoneNumber(FIND_PHONE).getPhone());
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC_URL);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Courier");
        int result = 0;
        while (rs.next())
            result++;
        if (null != rs)
            rs.close();
        if (null != stmt)
            stmt.close();
        if (null != conn)
            conn.close();
        assertEquals(result, courierBO.readAll().size());
    }

}
